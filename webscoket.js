
const TYPE_ENTER = 0;
const TYPE_LEAVE = 1;
const TYPE_MSG = 2;

let div = document.querySelector('div');
let btn = document.querySelector('button');
let inp = document.querySelector('input');


let ws = new WebSocket('ws://localhost:5500');

ws.addEventListener('open',()=>{
    console.log('连接成功');
    div.innerHTML = '连接成功'
})

btn.addEventListener('click',()=>{
    let val = inp.value;
    
    ws.send(val);
    inp.value = '';
})

ws.addEventListener('message',(event)=>{
    let result = JSON.parse(event.data);
    console.log(result);
    let newD = document.createElement('div');
    if(result.type === TYPE_ENTER){
        newD.style.color = 'green'
    }else if(result.type === TYPE_LEAVE){
        newD.style.color = 'red'
    }else{
        newD.style.color = 'blue'
    }

    newD.innerHTML = result.msg +' -------------- ' + result.time;
    div.append(newD)
})

ws.addEventListener('close',()=>{
    div.innerHTML = '连接已断开'
})