const ws = require('nodejs-websocket')

const TYPE_ENTER = 0;
const TYPE_LEAVE = 1;
const TYPE_MSG = 2;

let count = 0;

const server = ws.createServer(connect=>{
    console.log('新的连接');
    count++;
    connect.username = `用户${count}`

    broadcast({
        type:TYPE_ENTER,
        msg:`用户${connect.username}进入聊天室`,
        time:new Date().toLocaleTimeString()
    })

    connect.on('text',(data)=>{
        console.log('接受到数据'+data);
        broadcast({
            type:TYPE_MSG,
            msg:connect.username + ':' + data,
            time:new Date().toLocaleTimeString()
        })
    })

    connect.on('close',data=>{
        console.log('关闭连接');
        broadcast({
            type:TYPE_LEAVE,
            msg:`${connect.username}已离开聊天室`,
            time:new Date().toLocaleTimeString()
        })
    })

    connect.on('error',err=>{
        console.log('发生异常');
    })
    
  

}).listen(5500,()=>{
    console.log('5500端口开始监听');
})


//广播函数
function broadcast(msg){
    server.connections.forEach((connect)=>{
        connect.send(JSON.stringify(msg))
    })
}